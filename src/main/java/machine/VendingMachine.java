package machine;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import machine.util.FileLoader;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VendingMachine {

    private String menuFileLocation = "src/main/resources/menu.json";
    private String coinFileLocation = "src/main/resources/coins.json";
    private String bankFileLocation = "src/main/resources/state.json";

    private FileLoader fileLoader;
    private double bank;
    private List<Item> menu;
    private List<Double> allowedCoins;

    public VendingMachine() {
        init();
    }

    public VendingMachine(String menuFileLocation) {
        this.menuFileLocation = menuFileLocation;
        init();
    }

    private void init() {

        fileLoader = new FileLoader();
        menu = new ArrayList<>();
        allowedCoins = new ArrayList<>();

        String menuLoadingErrorMsg = "Menu can't be loaded!";
        String coinsLoadingErrorMsg = "Coins can't be loaded!";
        String stateLoadingErrorMsg = "State can't be loaded!";

        menu = (List<Item>) fileLoader.loadFile(
                menuFileLocation,
                menuLoadingErrorMsg,
                new TypeReference<List<Item>>() {
                });
        allowedCoins = (List<Double>) fileLoader.loadFile(
                coinFileLocation,
                coinsLoadingErrorMsg,
                new TypeReference<List<Double>>() {
                });
        bank = (Double) fileLoader.loadFile(
                bankFileLocation,
                stateLoadingErrorMsg,
                new TypeReference<Double>() {
                });

    }

    public void showMenu() {
        System.out.println("(# , Name, Cost) MENU:");
        for (int i = 0; i < menu.size(); i++) {
            Item currentItem = getMenuItemById(i);
            System.out.println(i + " " + currentItem.getName() + " " + currentItem.getCost());
        }
    }

    public Item getMenuItemById(int id) {
        return menu.get(id);
    }

    public boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean checkCommand(String input) {
        if (input.equals("exit")) {
            System.out.println("Bye!");
            return true;
        }
        return false;
    }

    public void start() {

        Scanner in = new Scanner(System.in);
        boolean askForMenuItem = true;
        while (askForMenuItem) {
            showMenu();
            System.out.print("Enter menu item number: ");
            String input = in.next();
            if (checkCommand(input)) {
                return;
            }
            if (!isNumeric(input)) {
                System.out.println("Not a number.");
                continue;
            }
            int menuItemNumber = Integer.parseInt(input);
            if (menu.size() < menuItemNumber) {
                System.out.println("Wrong menu item number.");
                continue;
            }
            Item currentItem = menu.get(menuItemNumber);
            System.out.println("Selected: " + currentItem.getName() + "(" + currentItem.getCost() + ")");
            System.out.println("Allowed coins: " + allowedCoins);
            boolean askForCoins = true;
            double enteredAmount = 0;
            while (askForCoins) {
                System.out.println("Enter coin(or cancel):");
                input = in.next();
                if (input.equals("cancel")) {
                    System.out.println("Change: " + enteredAmount);
                    enteredAmount = 0;
                    break;
                }
                if (checkCommand(input)) {
                    return;
                }
                if (!isNumeric(input)) {
                    System.out.println("Not a number.");
                    continue;
                }
                double inputInDouble = Double.parseDouble(input);
                if (!allowedCoins.contains(inputInDouble)) {
                    System.out.println("Not allowed number.");
                    continue;
                }
                enteredAmount += inputInDouble;
                double result = currentItem.getCost() - enteredAmount;
                askForCoins = checkCoins(result, currentItem.getName());
            }
            if (enteredAmount != 0) {
                bank += currentItem.getCost();
                saveState();
            }
        }
    }

    public boolean checkCoins(double result, String name) {
        if (result == 0.0) {
            System.out.println("Here you go take " + name);
            return false;
        }
        if (result < 0.0) {
            System.out.println("Here you go take " + name);
            System.out.println("Change: " + Math.abs(result));
            return false;
        }
        if (result > 0.0) {
            System.out.println("Enter more coins");
        }
        System.out.println("Something went wrong");
        return true;
    }

    public double showBank() {
        System.out.println("There is " + bank + " in this machine");
        return bank;
    }

    public void saveState() {
        try {
            new ObjectMapper().writeValue(Paths.get("src/main/resources/state.json").toFile(), bank);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
