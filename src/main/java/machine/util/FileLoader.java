package machine.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Paths;

public class FileLoader {

    public Object loadFile(String menuFileLocation, String errorMsg, TypeReference typeReference) {
        try {
            return new ObjectMapper().readValue(
                    Paths.get(menuFileLocation).toFile(),
                    typeReference);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(errorMsg);
        }
        return null;
    }

}
