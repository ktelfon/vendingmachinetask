package machine;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VendingMachineTest {

    private VendingMachine machine;

    @BeforeEach
    void setUp() {
        machine = new VendingMachine("src/test/resources/menu.json");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void isNumeric() {

        assertEquals(true, machine.isNumeric("1"));
        assertEquals(false, machine.isNumeric("sadfsadf"));
    }

    @Test
    void checkCommand() {
        assertEquals(true, machine.checkCommand("exit"));
        assertEquals(false, machine.checkCommand("sadfsadf"));
    }

    @Test
    void getMenuItemByIdTest() {
        assertEquals("Chips", machine.getMenuItemById(0).getName());
        assertEquals(1.5, machine.getMenuItemById(0).getCost());
    }

    @Test
    void checkCoins() {
        assertEquals(false, machine.checkCoins(0, ""));
        assertEquals(false, machine.checkCoins(-1, ""));
        assertEquals(true, machine.checkCoins(1, ""));
    }
}